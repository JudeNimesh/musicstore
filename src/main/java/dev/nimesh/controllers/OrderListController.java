package dev.nimesh.controllers;

import dev.nimesh.models.OrderList;
import dev.nimesh.services.MusicStoreService;
import dev.nimesh.services.OrderListService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderListController {

    private Logger logger = LoggerFactory.getLogger(OrderListController.class);
    private OrderListService service = new OrderListService();

    // this method handles GET /items
    public void handleGetOrderRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            // ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }

    public void handleGetOrderByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            OrderList order = service.getById(idInput);
            if (order == null) {
                //ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(order);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewOrder(Context ctx){
        OrderList order = ctx.bodyAsClass(OrderList.class);
        logger.info("adding new item: "+order);
        service.add(order);
        ctx.status(201);
    }

    public void handleDeleteOrderById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateOrderById(Context ctx) {
        OrderList newOrder = ctx.bodyAsClass(OrderList.class);
        logger.info("replacing a new order: "+newOrder);

        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            OrderList order = service.getById(idInput);
            if (order == null) {
                // ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing item: "+order);

                service.update(order, newOrder);
                logger.info("After Updating to a new item(on same index): "+newOrder);
                ctx.json(newOrder);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
