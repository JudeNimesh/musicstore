package dev.nimesh.controllers;

import dev.nimesh.models.UserList;
import dev.nimesh.services.UserService;
import dev.nimesh.services.MusicStoreService;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {

    private Logger logger = LoggerFactory.getLogger(CustomerControler.class);
    private UserService service = new UserService();

    // this method handles GET /items
    public void handleGetUserRequest(Context ctx){

       // /items with no query params
       //ctx.result(data.getAllItems().toString());}
       logger.info("getting all items");
         ctx.json(service.getAll()); // json method converts object to JSON

    }

    public void handleGetUserByIdRequest(Context ctx) {
        String usernameStr = ctx.pathParam("username");

        if(usernameStr.matches("^\\s+$")){
            UserList item2 = service.getUserListById(usernameStr);
            logger.info("600: Customer username: "+ usernameStr+"\n"+item2.toString());
            if (item2 == null) {
                //                ctx.status(404);
                logger.warn("no item present with id: " + usernameStr);
                throw new NotFoundResponse("No item found with provided username: " + usernameStr);
            } else {
                logger.info("getting item with id: " + usernameStr);
                ctx.json(item2);
            }
        } else {
            throw new BadRequestResponse("input \""+usernameStr+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewUser(Context ctx){
        UserList user = ctx.bodyAsClass(UserList.class);
        logger.info("adding new user: "+user);
        service.add(user);
        ctx.status(201);
    }

    public void handleDeleteUserById(Context ctx){
        String usernameStr = ctx.pathParam("username");
        if(usernameStr.matches("^\\s+$")){
            logger.info("deleting record with id: "+usernameStr);
            service.delete(usernameStr);
        } else {
            throw new BadRequestResponse("input \""+usernameStr+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateUserById(Context ctx) {
        UserList newUser = ctx.bodyAsClass(UserList.class);
        logger.info("replacing a new user: "+newUser);

        String usernameStr = ctx.pathParam("username");
        if(usernameStr.matches("^\\s+$")){
            logger.info("checking id got captured in int username: " + usernameStr); //test

            UserList user = service.getUserListById(usernameStr);
            if (user == null) {
                // ctx.status(404);
                logger.warn("no user present with username: " + usernameStr);
                throw new NotFoundResponse("No item found with provided ID: " + usernameStr);
            } else {
                logger.info("Before updating an existing user: "+user);

                service.update(user, newUser);
                logger.info("After Updating to a new user(on same index): "+newUser);
                ctx.json(newUser);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+usernameStr+"\" cannot be parsed to an int");
        }
    }

}
