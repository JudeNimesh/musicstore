package dev.nimesh.controllers;

import dev.nimesh.models.CustomerList;
import dev.nimesh.services.CustomerService;
import dev.nimesh.services.MusicStoreService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerControler {

    private Logger logger = LoggerFactory.getLogger(CustomerControler.class);
    private CustomerService service = new CustomerService();

    // this method handles GET /items
    public void handleGetCustomerRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            //           ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }

    public void handleGetCustomerByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            CustomerList item2 = service.getCustomerListById(idInput);
            logger.info("600: Customer ID: "+ idInput+"\n"+item2.toString());
            if (item2 == null) {
                //                ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(item2);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewCustomer(Context ctx){
        CustomerList item = ctx.bodyAsClass(CustomerList.class);
        logger.info("adding new item: "+item);
        service.add(item);
        ctx.status(201);
    }

    public void handleDeleteCustomerById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateCustomerById(Context ctx) {
        CustomerList newUser = ctx.bodyAsClass(CustomerList.class);
        logger.info("replacing a new item: "+newUser);

        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            CustomerList user = service.getCustomerListById(idInput);
            if (user == null) {
                // ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing user: "+user);

                service.update(user, newUser);
                logger.info("After Updating to a new user(on same index): "+newUser);
                ctx.json(newUser);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
