package dev.nimesh.models;

import java.io.Serializable;
import java.util.Objects;

public class MusicProduct implements Serializable {

    private int prod_id;
    private String prod_name;
    public String deprt_name;
    private double price;
    private int stock;


    public MusicProduct(){
        super();
    }

    public MusicProduct(int prod_id,  String prod_name, String deprt_name, double price, int stock ) {
        this.prod_id = prod_id;
        this.prod_name = prod_name;
        this.price = price;
        this.stock = stock;
        this.deprt_name = deprt_name;
    }

    //public MusicProduct(int prod_id) { this.prod_id = prod_id; }

    public int getProdId() { return prod_id; }

    public void setProdId(int id) { this.prod_id = prod_id; }

    public String getProdName() { return prod_name; }

    public void setProdName(String name) { this.prod_name = name; }

    public String getDepartmentName() { return deprt_name; }

    public void setDepartmentName(String deprt_name) { this.deprt_name = deprt_name; }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) { this.stock = stock; }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) { this.price = price; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MusicProduct product = (MusicProduct) o;
        return prod_id == product.prod_id && Double.compare(product.price, price) == 0 && stock == product.stock
                && Objects.equals(prod_name, product.prod_name) && Objects.equals(deprt_name, product.deprt_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prod_id, prod_name, deprt_name, price, stock);
    }

    @Override
    public String toString() {
        return "MusicProduct{" +
                "prod_id=" + prod_id +
                ", prod_name='" + prod_name + '\'' +
                ", deprt_name='" + deprt_name + '\'' +
                ", price=" + price + '\''+
                ", stock=" + stock + "\n"+'\'' +
                '}';
    }
}
