package dev.nimesh.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(connection == null || connection.isClosed()) {
            String connectionUrl = System.getenv("connectionUrl");
            String username = System.getenv("username");
            String password = System.getenv("password");

            // create a connection
            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }


    public static Connection getHardcodedConnection() throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if(connection == null || connection.isClosed()) {

            String server = "nimesh-java-azure-training";
            String url      = server+".postgres.database.azure.com";

            String connectionUrl = "jdbc:postgresql://"+url+":5432/postgres";
            String username = "jnimesh@nimesh-java-azure-training";
            String password = "xxxxxx";

            // create a connection
            //connection = DriverManager.getConnection(connectionUrl, username, password);
            connection = DriverManager.getConnection(connectionUrl, username, password);

        }
        return connection;
    }



}
