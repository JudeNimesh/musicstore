package dev.nimesh;

import dev.nimesh.controllers.*;
import dev.nimesh.data.MusicProductData;
import dev.nimesh.models.MusicProduct;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

import static io.javalin.apibuilder.ApiBuilder.*;

public class MusicProductDriver {

    public static void main(String[] args) {

        Javalin app = Javalin.create().start(7000);
//        app.get("/", (Context ctx)-> {ctx.result("Hello World");});
//        app.get("/", ctx -> ctx.result("Hello World"));

        ProductController productController = new ProductController();
        OrderListController orderListController = new OrderListController();
        CustomerControler customerControler = new CustomerControler();
        AuthController authController = new AuthController();
        UserController userController = new UserController();


        app.routes(()->{

            path("/items", ()-> {
                before("/", authController::authorizeToken);
                get(productController::handleGetProductRequest);
                post(productController::handlePostNewProduct);

                path("/:id", () -> {
                    before("/", authController::authorizeToken);
                    get(productController::handleGetProductByIdRequest);
                    delete(productController::handleDeleteProductById);
                    put(productController::handleUpdateProductById);
                });
            });

            before("/users", authController::authorizeToken);
            path("/users", ()-> {
                before("/", authController::authorizeToken);
                get(userController::handleGetUserRequest);
                post(userController::handlePostNewUser);
                path("/:id", () -> {
                    before("/", authController::authorizeToken);
                    get(userController::handleGetUserByIdRequest);
                    delete(userController::handleDeleteUserById);
                    put(userController::handleUpdateUserById);
                });
            });

            path("/customers", ()-> {
                before("/", authController::authorizeToken);
                get(customerControler::handleGetCustomerRequest);
                post(customerControler::handlePostNewCustomer);
                path("/:id", () -> {
                    before("/", authController::authorizeToken);
                    get(customerControler::handleGetCustomerByIdRequest);
                    delete(customerControler::handleDeleteCustomerById);
                    put(customerControler::handleUpdateCustomerById);
                });
            });



            path("/orders", ()->{
                before("/", authController::authorizeToken);
                get(orderListController::handleGetOrderRequest);
                post(orderListController::handlePostNewOrder);
                path("/:id",()->{
                    before("/", authController::authorizeToken);
                    get(orderListController::handleGetOrderByIdRequest);
                    delete(orderListController::handleDeleteOrderById);
                    put(orderListController::handleUpdateOrderById);
                });
            });

            path("/login",()->{
                post(authController::authenticateLogin);

            });
        });



    }

}
