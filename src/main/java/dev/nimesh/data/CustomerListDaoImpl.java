package dev.nimesh.data;

import dev.nimesh.models.CustomerList;
import dev.nimesh.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerListDaoImpl implements  CustomerListDao {

    private Logger logger = LoggerFactory.getLogger(OrderListDaoImpl.class);

    @Override
    public ArrayList<CustomerList> getAllCustomers() {
        ArrayList<CustomerList> users = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from customer_list");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int cust_id      = resultSet.getInt("id");
                String cust_name = resultSet.getString("cust_name");
                String address   = resultSet.getString("address");
                String email     = resultSet.getString("email");
                String phone     = resultSet.getString("phone");
                String username  = resultSet.getString("username");
                //String password  = resultSet.getString("password");

logger.info("TEST: 200 cust_id"+ cust_id+", "+cust_name+", "+address+", "+email+", "+phone+", "+username);
                CustomerList user = new CustomerList(cust_id,cust_name,address,email,phone,username);
                users.add(user);
            }
            logger.info("selecting all users from db - "+ users.size()+ " users retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return users;
    }

    @Override
    public CustomerList getCustomerById(int cust_id) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from customer_list where id = ?");
            prepareStatement.setInt(1,cust_id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String cust_name = resultSet.getString("cust_name");
                String address   = resultSet.getString("address");
                String email     = resultSet.getString("email");
                String phone     = resultSet.getString("phone");
                String username  = resultSet.getString("username");
                //String password  = resultSet.getString("password");

                logger.info("1 user retrieved from database by cust_id: "+cust_id+ ", cust_name: "+cust_name+
                ", address: "+address+", email: "+email+ ", phone: "+phone+", username: "+username);
                logger.info("700 phone: "+ phone);
                System.out.println("701: Customer Info:"+ new CustomerList(cust_id,cust_name,address,email,phone,username));

                return new CustomerList(cust_id,cust_name,address,email,phone,username);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public CustomerList addNewCustomer(CustomerList user) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement =
             connection.prepareStatement("insert into customer_list (id, cust_name, address, email, phone, username) " +
                     "values (?, ?, ?, ?, ?, ?)");
            preparedStatement.setInt(1, user.getCustomerId());
            preparedStatement.setString(2, user.getCustomerName());
            preparedStatement.setString(3, user.getAddress());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setString(6, user.getUsername());
            //preparedStatement.setString(6, user.getPassword());
logger.info("TEST 700: New custemer added user.getCustomerId()="+ user.getCustomerId()+" "+user.getCustomerName()+" "+
        user.getPhone()+" "+user.getUsername() );
            preparedStatement.executeUpdate();
            logger.info("successfully added new user to the db");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return user;
    }

    @Override
    public void deleteCustomer(int cust_id) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from customer_list where id = ?"); //can use name, price
            preparedStatement.setInt(1, cust_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String cust_name = resultSet.getString("cust_name");
                String address   = resultSet.getString("address");
                String email     = resultSet.getString("email");
                String phone     = resultSet.getString("phone");
                String username  = resultSet.getString("username");
                //String password  = resultSet.getString("password");

                logger.info("1 user deleted from database by cust_id: " +cust_id+ ", cust_name: "+cust_name+
                        ", address: "+address+", email: "+email+ ", phone: "+phone+", username: "+username);
                new CustomerList(cust_id,cust_name,address,email,phone,username);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
    }

    @Override
    public CustomerList updateCustomer(CustomerList oldUser, CustomerList newUser) {
        try (Connection connection = ConnectionUtil.getConnection()) {

            int cust_id = oldUser.getCustomerId();

            String cust_name = newUser.getCustomerName();
            String address   = newUser.getAddress();
            String email     = newUser.getEmail();
            String phone     = newUser.getPhone();
            String username  = newUser.getUsername();
            //String password  = newUser.getPassword();

            PreparedStatement preparedStatement = connection.prepareStatement("select * from customer_list where id = ?"); //can use name, price
            preparedStatement.setInt(1, cust_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                String oldcust_name = resultSet.getString("cust_name");
                String oldaddress   = resultSet.getString("address");
                String oldemail     = resultSet.getString("email");
                String oldphone     = resultSet.getString("phone");
                String oldusername  = resultSet.getString("username");
                //String oldpassword  = resultSet.getString("password");

                logger.info("1 user retrieved from database by cust_id: " +cust_id+ ", cust_name: "+cust_name+
                        ", address: "+address+", email: "+email+ ", phone: "+phone+", username: "+username);

                PreparedStatement preparedStatement2 =
                        connection.prepareStatement("update customer_list set cust_name = ?, address = ?, " +
                                "email = ?, phone = ?, username = ? where id = ?"); //can use name, price

                preparedStatement2.setString(1, cust_name);
                preparedStatement2.setString(2, address);
                preparedStatement2.setString(3, email);
                preparedStatement2.setString(4, phone);
                preparedStatement2.setString(5, username);
                //preparedStatement2.setString(6, password);
                preparedStatement2.setInt(6, cust_id);

                preparedStatement2.executeUpdate();
                logger.info("successfully updated a new item to the db");
                logger.info("1 user updated to the database by cust_id: " +cust_id+ ", cust_name: "+cust_name+
                        ", address: "+address+", email: "+email+ ", phone: "+phone+", username: "+username);
                return new CustomerList(cust_id,cust_name,address,email,phone,username);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return null;
    }
}
