package dev.nimesh.data;

import dev.nimesh.models.CustomerList;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.models.OrderList;
import dev.nimesh.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderListDaoImpl implements  OrderListDao {

    private Logger logger = LoggerFactory.getLogger(OrderListDaoImpl.class);

    //@Override
    public List<OrderList> getAllOrders() {
        List<OrderList> orders = new ArrayList<>();
        CustomerList customer = new CustomerList();
        MusicProduct product = new MusicProduct();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select order_id, order_name, cust_id, "+
                    " cust_name, prod_id, prod_name, price, qty, stock, total, deprt_name, address, email, phone,"+
                    " username from customer_list, order_list, product_items " +
                    "where  customer_list.id = order_list.cust_id and order_list.prod_id = product_items.id");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                // calculate the total sales value for each item purchased
                //process each record in the result set
                int order_id      = resultSet.getInt("order_id");
                String order_name = resultSet.getString("order_name");
                int cust_id       = resultSet.getInt("cust_id");
                String cust_name  = resultSet.getString("cust_name");
                int prod_id       = resultSet.getInt("prod_id");
                String prod_name  = resultSet.getString("prod_name");
                double price      = resultSet.getDouble("price");
                int qty           = resultSet.getInt("qty");
                double total      = qty * price;
                int stock           = resultSet.getInt("stock");
                String deprt_name = resultSet.getString("deprt_name");
                String address    = resultSet.getString("address");
                String email = resultSet.getString("email");
                String phone = resultSet.getString("phone");
                String username = resultSet.getString("username");

                customer = new CustomerList(cust_id, cust_name, address, email, phone,username);
                product  = new MusicProduct(prod_id, prod_name, deprt_name, price, stock);

                OrderList order = new  OrderList(order_id,order_name,cust_id,prod_id,qty,total, customer, product);
                orders.add(order);
                logger.info("TEST: 400 order_id "+ order_id+", "+order_name+", "+cust_id+", "+cust_name+", "+prod_id+", "+prod_name+
                            ", "+ price+", "+qty+", "+ total+", "+deprt_name+", "+address+", "+email+", "+phone);

            }
            logger.info("selecting all orders from db - "+ orders.size()+ " orders retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return orders;
    }

    @Override
    public OrderList getOrderById(int order_id) {
        CustomerList customer = new CustomerList();
        MusicProduct product = new MusicProduct();
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select order_id, "+
                    " order_name, cust_id, cust_name, prod_id, prod_name, price, qty, stock, total, "+
                    " deprt_name, address, email, phone,username from customer_list, order_list, " +
                    " product_items where  customer_list.id = order_list.cust_id "+
                    " and order_list.prod_id = product_items.id and order_id = ?");

            prepareStatement.setInt(1,order_id);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String order_name = resultSet.getString("order_name");
                int cust_id       = resultSet.getInt("cust_id");
                String cust_name  = resultSet.getString("cust_name");
                int prod_id       = resultSet.getInt("prod_id");
                String prod_name  = resultSet.getString("prod_name");
                double price      = resultSet.getDouble("price");
                int qty           = resultSet.getInt("qty");
                int stock         = resultSet.getInt("stock");
                double total      = qty * price;

                String deprt_name = resultSet.getString("deprt_name");
                String address    = resultSet.getString("address");
                String email = resultSet.getString("email");
                String phone = resultSet.getString("phone");
                String username = resultSet.getString("username");

                logger.info("1 order retrieved from database by order_id: " + order_id);

                customer = new CustomerList(cust_id, cust_name, address, email, phone,username);
                product  = new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
                return new OrderList(order_id,order_name,cust_id,prod_id,qty,total, customer,product);

            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public OrderList addNewOrder(OrderList order) {
        CustomerList customer = new CustomerList();
        MusicProduct product  = new MusicProduct();
        boolean customerNotFound = false;
        try(Connection connection = ConnectionUtil.getConnection()){
            connection.setAutoCommit(false);
            // Check if purchase user/customer is registered user
            String cust_name = "",address = "", email = "", phone = "", deprt_name = "";
            String prod_name = "",username = "";
            double price = 0.0;
            int stock    = 0, prod_id = 0, cust_id = 0, newProd_id, newCust_id=0;
            PreparedStatement prepStat1 = connection.prepareStatement( "select * from customer_list where email = ?");

            prepStat1.setString(1,order.getCustomer().getEmail());
            ResultSet rs1 = prepStat1.executeQuery();
            while(rs1.next()) {
                cust_id   = rs1.getInt("cust_id");
                cust_name = rs1.getString("cust_name");
                address   = rs1.getString("address");
                email     = rs1.getString("email");
                phone     = rs1.getString("phone");
                username     = rs1.getString("username");

                if (! email.equals(order.getCustomer().getEmail())) {
                    customerNotFound = true;
                } else {
                    customerNotFound = false;
                    newCust_id = cust_id;
                    customer = new CustomerList(cust_id, cust_name, address, email, phone,username);
                }
            }
            if(customerNotFound ){
                logger.warn("**** Warning **** --> Customer is NOT registered to do this purchase: "+cust_name);
                logger.info("Please use a Registered Customer to do this purchase!\n\n");
                return null;
            }

            // Check if purchase item/product is registered item
            PreparedStatement prepStat2 = connection.prepareStatement( "select * from product_items where id = ?");
            prepStat2.setInt(1,order.getProductId());
            ResultSet rs2 = prepStat1.executeQuery();
            while(rs2.next()) {
                prod_id   = rs2.getInt("id");
                prod_name = rs2.getString("prod_name");
                prod_id = rs2.getInt("prod_id");
                price = rs2.getDouble("price");
                stock = rs2.getInt("stock");
                deprt_name = rs2.getString("depart_name");

                if (prod_id != order.getProductId()) {
                    customerNotFound = true;
                } else {
                    customerNotFound = false;
                    newProd_id = prod_id;
                    product  = new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
                }
            }

            if ( customerNotFound) {
                logger.warn("**** Warning **** --> Customer is NOT registered to do this purchase: "+prod_name);
                logger.info("Please use a Registered Customer to do this purchase!\n\n");
                return null;
            }

            PreparedStatement prepStat3 = connection.prepareStatement( "insert into order_lists "+
               " (order_name, cust_id, prod_id, qty, total) values (?, ?, ?, ?, ? )");

            String order_name = order.getOrderName();
            int qty           = order.getOrderQuantity();
            double total      = qty * order.getMusicProduct().getPrice();
            cust_id           = order.getCustomer().getCustomerId();
            prod_id           = order.getProductId();

            prepStat3.setString(1, order_name );
            prepStat3.setInt(2,qty );
            prepStat3.setDouble(3, qty * total);
            prepStat3.setInt(4, cust_id );
            prepStat3.setInt(5, prod_id );
            prepStat3.executeUpdate();

            // Check if purchase order was updated and is registered in the database
            PreparedStatement prepStat4 = connection.prepareStatement( "select * from order_lists where order_name = ?");
            prepStat4.setString(1, order_name);
            ResultSet rs3 = prepStat4.executeQuery();

            int order_id   = rs3.getInt("order_id");

            connection.getAutoCommit();
            logger.info("successfully added new item to the db");
            logger.info("new order");

            //return new OrderList(order_id,order_name,qty,total, prod_id,prod_name,
            //        price, cust_id, cust_name, deprt_name, address, email, phone);
            return new OrderList(order_id,order_name,cust_id,prod_id,qty,total, customer,product);

        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

        logger.warn(" you can NOT make this Purchase: your NOT registered or product is not available");
        return null;
    }

    @Override
    public void deleteOrder(int order_id) {

        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from order_list where order_id = ?"); //can use name, price
            preparedStatement.setInt(1, order_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String order_name = resultSet.getString("order_name");
                int cust_id       = resultSet.getInt("cust_id");
                String cust_name  = resultSet.getString("cust_name");
                int prod_id       = resultSet.getInt("prod_id");
                String prod_name  = resultSet.getString("prod_name");
                double price      = resultSet.getDouble("price");
                int qty           = resultSet.getInt("qty");
                double total      = qty * price;
                String deprt_name = resultSet.getString("deprt_name");
                String address    = resultSet.getString("address");
                String email = resultSet.getString("email");
                String phone = resultSet.getString("phone");

                logger.info("1 order deleted from database by order_id: " + order_id);
                new OrderList(order_id,order_name,cust_id,prod_id,qty,total);
            }
        } catch (SQLException e) {
        logger.error(e.getClass() + "  " + e.getMessage());
        }
    }

    @Override
    public OrderList updateOrder(OrderList oldOrder, OrderList newOrder) {

        CustomerList customer = new CustomerList();
        MusicProduct product = new MusicProduct();
        boolean customerNotFound = false;

        try (Connection connection = ConnectionUtil.getConnection()) {
            //here only existing product can be selected and quantities can be changed and
            //customer info and product info are not changed.
            //===============================================
            int oldrder_id    = oldOrder.getOrderId();

            //-------------------------------------------------------------------------
            connection.setAutoCommit(false);
            // Check if user/customer is registered user to do this purchase this product
            String cust_name = "",address = "", email = "", phone = "", deprt_name = "";
            String prod_name = "", username = "";
            double price = 0.0;
            int stock    = 0, prod_id = 0, cust_id = 0, newProd_id, newCust_id=0;

            PreparedStatement prepStat1 = connection.prepareStatement( "select * from customer_list where email = ?");

            prepStat1.setString(1,oldOrder.getCustomer().getEmail());
            ResultSet rs1 = prepStat1.executeQuery();
            while(rs1.next()) {
                cust_id   = rs1.getInt("cust_id");
                cust_name = rs1.getString("cust_name");
                address   = rs1.getString("address");
                email     = rs1.getString("email");
                phone     = rs1.getString("phone");
                username     = rs1.getString("username");

                if (! email.equals(oldOrder.getCustomer().getEmail())) {
                    customerNotFound = true;
                } else {
                    customerNotFound = false;
                    newCust_id = cust_id;
                    customer = new CustomerList(cust_id, cust_name, address, email, phone,username);
                }
            }
            if(customerNotFound ){
                logger.warn("**** Warning **** --> Customer is NOT registered to do this purchase: "+cust_name);
                logger.info("Please first Registered the Customer to do this purchase!\n\n");
                return null;
            }

            // Check if purchase item/product is registered item
            PreparedStatement prepStat2 = connection.prepareStatement( "select * from product_items where id = ?");
            prepStat2.setInt(1, oldOrder.getProductId());
            ResultSet rs2 = prepStat2.executeQuery();
            while(rs2.next()) {
                prod_id   = rs2.getInt("id");
                prod_name = rs2.getString("prod_name");
                prod_id   = rs2.getInt("prod_id");
                price     = rs2.getDouble("price");
                stock     = rs2.getInt("stock");
                deprt_name = rs2.getString("depart_name");

                if (prod_id != oldOrder.getProductId()) {
                    customerNotFound = true;
                } else {
                    customerNotFound = false;
                    newProd_id = prod_id;
                    product  = new MusicProduct(prod_id, prod_name, deprt_name, price, stock);
                }
            }

            if ( customerNotFound) {
                logger.warn("**** Warning **** --> Customer is NOT registered to do this purchase: "+prod_name);
                logger.info("Please Registered product first and the try to purchase  this product!\n\n");
                return null;
            }

            PreparedStatement prepStat3 = connection.prepareStatement( "insert into order_lists "+
                    " (order_name, cust_id, prod_id, qty, total) values (?, ?, ?, ?, ? )");

            int order_id     = oldrder_id;  //old order id is updated with new products and qty
            String order_name = newOrder.getOrderName();
            int qty           = newOrder.getOrderQuantity();
            double total      = qty * newOrder.getMusicProduct().getPrice();
            cust_id           = newOrder.getCustomer().getCustomerId();
            prod_id           = newOrder.getProductId();

            prepStat3.setString(1, order_name );
            prepStat3.setInt(2,qty );
            prepStat3.setDouble(3, qty * total);
            prepStat3.setInt(4, cust_id );
            prepStat3.setInt(5, prod_id );
            prepStat3.executeUpdate();

            connection.getAutoCommit();

            logger.info("new order");
            logger.info("successfully updated a new item to the db");
            logger.info("1 order updated to the database by order_id: " +order_id+", order_name: "+order_name+
                        ", prod_name: prod_name, price: "+price+", qty: " +qty+", cust_name: "+cust_name+
                        ", deprt_name: deprt_name, email: " +email+", phone: " +phone+", address: "+address);

                return new OrderList(order_id,order_name,cust_id,prod_id,qty,total, customer,product);

        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return null;
    }
}
