package dev.nimesh.data;

import dev.nimesh.models.OrderList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
    this was our original data access method with collections handling in memory items
 */

public class OrderListData implements OrderListDao {

    private List<OrderList> orders = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(OrderListData.class);

    public OrderListData(){
        super();

     //   orders.add(new OrderList(10,"Gift",2,"Jeba", 15,"Guitar","Instrument", 650, 24));
     //   orders.add(new OrderList(5,"School",1,"Camera",11,"Video", 875.90, 10));
     //   orders.add(new OrderList(10,"VTA",3,"Mixer",12,"Audio", 120, 5));
     //   orders.add(new OrderList(10,"Spot Beam",2,"Lighting",13, 1625.45, 3));

    }
    public List<OrderList> getAllOrders(){
        return new ArrayList<>(orders);
    }

    public OrderList getOrderById(int order_id){
        /*
        for(OrderList order : orders){
            if(order_id==order.getId()){
                return order;
            }
        }
        return null;
         */
        return orders.stream().filter(order->order.getOrderId()==order_id).findAny().orElse(null);
    }

    public OrderList addNewOrder(OrderList order){
        orders.add(order);
        return order;
    }

    public void deleteOrder(int order_id){
//      orders.removeIf(orderList -> (orderList!=null)?order_id==orderList.getId():false);
        Predicate<OrderList> idCheck = orderList -> orderList!=null && order_id==orderList.getOrderId();
        orders.removeIf(idCheck);
        // this remove operation can also be handled using iteration, looping through and using the
        // list remove method
    }
    public OrderList  updateOrder(OrderList order, OrderList newOrder) {

        OrderList oldOrder = orders.stream().filter(orderList -> orderList.getOrderId() == order.getOrderId()).findAny().orElse(null);
        logger.info("Before Updating to an old order: " + oldOrder);
        logger.info("The new order replace on the indexOf(oldOrder)=" + orders.indexOf(oldOrder) + "<-- new order: " + newOrder);
        orders.set(orders.indexOf(oldOrder), newOrder);

        logger.info("After Updating to a new order: " + newOrder.toString());

        return newOrder;  //
    }
}


