package dev.nimesh.services;

import dev.nimesh.data.MusicProductDao;
import dev.nimesh.data.MusicProductDaoImpl;
import dev.nimesh.models.MusicProduct;

import java.util.List;


public class MusicStoreService {

    private MusicProductDao musicProductDao = new MusicProductDaoImpl();

    public List<MusicProduct> getAll() {
        return musicProductDao.getAllProducts();
    }


    public MusicProduct getById(int id) {

        return musicProductDao.getProductById(id);
    }

    public MusicProduct add(MusicProduct item) {

        return musicProductDao.addNewProduct(item);
    }

    public void delete(int id) {

        musicProductDao.deleteProduct(id);
    }

    public void update(MusicProduct oldItem, MusicProduct newItem) {

        musicProductDao.updateProduct(oldItem, newItem);
    }


    public List<MusicProduct>  getPriceInRange(String minPriceStr, String maxPriceStr){
        double maxPrice;
        double minPrice;

        if( maxPriceStr != null) {
            // max provided
            // parse string maxPriceStr input to double
            maxPrice = Double.parseDouble(maxPriceStr);
            if (minPriceStr != null) {
                //if both are provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
//                return musicProductDao.getItemsInRange(minPrice, maxPrice); //(double min, double max);
            }
            //  only maxPrice provided
            // return musicProductDao.getItemWithMaxPrice( maxPrice ); // (double price );
        }else {
            if (minPriceStr != null) {
                // only min provided
                // parse string minPriceStr input to double
                minPrice = Double.parseDouble(minPriceStr);
                //   return musicProductDao.getItemWithMinPrice( minPrice);  // (double price );
            }
        }
        return null;
    }
}
